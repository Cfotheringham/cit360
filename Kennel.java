public class Kennel {

	public DogBean[] buildDogs() {
		DogBean[] dogs = new DogBean[5];
		DogBean dog1 = new DogBean(4, " brown", 5.6, "husky", "Fido");
		DogBean dog2 = new DogBean(4, " Black", 3.4, "lab", "Scruffy");
		DogBean dog3 = new DogBean(4, " white", 3.8, "poodle", "Fluffy");
		DogBean dog4 = new DogBean(4, " brown", 2.4, "Chihuahua", "rex");
		DogBean dog5 = new DogBean();
		dogs[0] = dog1;
		dogs[1] = dog2;
		dogs[2] = dog3;
		dogs[3] = dog4;
		dogs[4] = dog5;
		return dogs;
	}

	public void displayDogs(DogBean[] dogs) {
		for (int i = 0; i < dogs.length; i++) {
			System.out.println(dogs[i].toString());
		}
	}

	public static void main(String[] args) {
		Kennel buddyCenter = new Kennel();

		DogBean[] fidos = buddyCenter.buildDogs();
		
		buddyCenter.displayDogs(fidos);

	}
}
