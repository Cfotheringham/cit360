public class MammalBean{

	/* MammalBean's field */
	private int legCount;
	private String color;
	private double height;
	/* Getters and Setters for the Properties */

	public int getLegCount() {
		return legCount;
	}

	public void setLegCount(int legCount) {
		this.legCount = legCount;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	MammalBean() {
		legCount = 4;
		color = " brown";
		height = 5.6;
	}
	
	/* This is the constructor that accepts and sets values */
	MammalBean(int ls, String c, double h){
		legCount = ls;
		color = c;
		height = h;
	}


	
}