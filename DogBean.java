public class DogBean extends MammalBean{
		/* DogBean Adds some more private properties. */
		private String breed;
		private String name;

		DogBean(){
			breed = "Lab";
			name = "charlie";
		}
		/* This is a constructor that accepts and sets values 
		 * for each Dogbean/ MammalBEan property */

		DogBean(int ls, String c, double h, String b, String n) {
			super(ls, c, h);
			breed = b;
			name = n;
		}

		/* These are the getters and setters for each properties. */
		public String getBreed() {
			return breed;
		}

		public void setBreed(String breed) {
			this.breed = breed;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		/* This toString() method will print the properties. */
		@Override 
		public String toString() {
			return " The DogBean legCount is: " + this.getLegCount() + " The color is: " + this.getColor() + " The height is: "
					+ this.getHeight() + " The breed is: " + this.getBreed() + " The nameof the dog: " + this.getName();
		}

	
	}