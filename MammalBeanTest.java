import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Test;

public class MammalBeanTest {

	@Test
	public void testMammal() {
		
		//Create the first mammalBean Object and test it. 
		
		MammalBean mammal = new MammalBean (3, "black", 5.6);
		assertEquals (3, mammal.getLegCount());
		assertEquals ("black", mammal.getColor());
		assertEquals (5.6, mammal.getHeight(), 1e-4);
		
		//Create the second mammalBean Object and test it. 
		
		MammalBean mammal2 = new MammalBean (4, "brown", 4.0);
		assertEquals (4, mammal2.getLegCount());
		assertEquals ("brown", mammal2.getColor());
		assertEquals (4.0, mammal2.getHeight(), 1e-4);
		
		//Create the third test
		
		MammalBean mammal3 = new MammalBean (4, "black", 3.8);
		assertEquals (4, mammal3.getLegCount());
		assertEquals ("black", mammal3.getColor());
		assertEquals (3.8, mammal3.getHeight(), 1e-4);
		
		//Create the fourth test
		
		MammalBean mammal4 = new MammalBean (4, "white", 4.5);
		assertEquals (4, mammal4.getLegCount());
		assertEquals ("white", mammal4.getColor());
		assertEquals (4.5, mammal4.getHeight(), 1e-4);
	}
	public void testDog() {
		
		//Test 1
		
		DogBean dog1 = new DogBean (4,"brown", 4.6, "husky", "joe");
		assertEquals (4, dog1.getLegCount());
		assertEquals ("brown", dog1.getColor());
		assertEquals (4.6, dog1.getHeight(), 1e-4);
		assertEquals ("husky", dog1.getBreed());
		assertEquals ("joe", dog1.getName());
		
		//Test 2
		
		DogBean dog2 = new DogBean (4,"brown", 4.8, "lab", "scruffy");
		assertEquals (4, dog2.getLegCount());
		assertEquals ("brown", dog2.getColor());
		assertEquals (4.8, dog2.getHeight(), 1e-4);
		assertEquals ("lab", dog2.getBreed());
		assertEquals ("scruffy", dog2.getName());
		
		//Test 3
		
		DogBean dog3 = new DogBean (4,"white", 3.6, "poodle", "fluffy");
		assertEquals (4, dog3.getLegCount());
		assertEquals ("white", dog3.getColor());
		assertEquals (3.6, dog3.getHeight(), 1e-4);
		assertEquals ("poodle", dog3.getBreed());
		assertEquals ("fluffy", dog3.getName());
		
		//Test 4
		
		DogBean dog4 = new DogBean (4,"grey", 5.6, "Great Dane", "Dusty");
		assertEquals (4, dog4.getLegCount());
		assertEquals ("grey", dog4.getColor());
		assertEquals (5.6, dog4.getHeight(), 1e-4);
		assertEquals ("Great Dane", dog4.getBreed());
		assertEquals ("Dusty", dog4.getName());
	}
	
	@Test
	public void TestSet() {
		
	//Create a new HashSet
		
	HashSet TestMammal = new HashSet();
	
	//Create objects.
	
	MammalBean mammal = new MammalBean (3, "black", 5.6);
	MammalBean mammal2 = new MammalBean (4, "brown", 4.0);
	MammalBean mammal3 = new MammalBean (4, "black", 3.8);
	MammalBean mammal4 = new MammalBean (4, "white", 4.5);
	
	//add objects to HashSet
	
	TestMammal.add(mammal);
	TestMammal.add(mammal2);
	TestMammal.add(mammal3);
	TestMammal.add(mammal4);
	
	//Test if they are in the HashSet
	
	assertEquals(true, TestMammal.contains(mammal));
	assertEquals(true, TestMammal.contains(mammal2));
	assertEquals(true, TestMammal.contains(mammal3));
	assertEquals(true, TestMammal.contains(mammal4));
	
	//Take them out of HashSet
	
	TestMammal.remove(mammal);
	TestMammal.remove(mammal3);
	
	//Test if they are now out of hashSet
	
	assertEquals(false, TestMammal.contains(mammal));
	assertEquals(true, TestMammal.contains(mammal2));
	assertEquals(false, TestMammal.contains(mammal3));
	assertEquals(true, TestMammal.contains(mammal4));
}
	@Test
	public void TestDog() {
		
		//Create a Map 
		
		Map <String , DogBean > Dog = new HashMap <String, DogBean>(); 
		
		//Create Dog Objects
		
		DogBean dog1 = new DogBean (4,"brown", 4.6, "husky", "joe");
		DogBean dog2 = new DogBean (4,"brown", 4.8, "lab", "scruffy");
		DogBean dog3 = new DogBean (4,"white", 3.6, "poodle", "fluffy");
		DogBean dog4 = new DogBean (4,"grey", 5.6, "Great Dane", "Dusty");
		
		//Put the Dog objects in HashMap
		
		Dog.put("joe", dog1);
		Dog.put("scruffy", dog2);
		Dog.put("fluffy", dog3);
		Dog.put("Dusty", dog4);
		
		//Test if in HashMap
		
		assertEquals(true, Dog.containsKey("joe"));
		assertEquals(true, Dog.containsKey("scruffy"));
		assertEquals(true, Dog.containsKey("fluffy"));
		assertEquals(true, Dog.containsKey("Dusty"));
		
		//Take out objects.
		
		Dog.remove("scruffy", dog2);
		Dog.remove("Dusty", dog4);
		
		//Test to see if removed
		
		assertEquals(true, Dog.containsKey("joe"));
		assertEquals(false, Dog.containsKey("scruffy"));
		assertEquals(true, Dog.containsKey("fluffy"));
		assertEquals(false, Dog.containsKey("Dusty"));
		
	}
}